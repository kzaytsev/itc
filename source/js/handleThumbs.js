/**
 * Created by kzaytsev on 11/17/2015.
 */
(function ($) {
  var methods = {
    init: function () {

      var $me = $(this);

      handleThumbs($me);
      $(window).on('resize', function () {
        setTimeout(function () {
          onWindowResize($me)
        }, 50);
      });
    }

  };

  /*PRIVATE METHODS*/

  /* Handlers */

  function handleThumbs($me) {
    var thumbsContainers = getThumbs($me);

    var $thumbColumns = $me.find('.app-grid__column_thumbs');
    var fullHeight = getClientHeight();

    var containersHeight = fullHeight - 150;


    if (thumbsContainers.last().children().length == 0) {
      $thumbColumns.last().hide();
    } else {
      $thumbColumns.last().show();
    }

    $.each(thumbsContainers, function () {
      $(this).css({height: containersHeight});
    });
  }

  /* Getters */

  function getThumbs($me) {
    return $me.find('.app-thumbs__container');
  }

  function getClientHeight() {
    return $(window).height();
  }

  /* Events */

  function onWindowResize($me) {
    handleThumbs($me);
  }

  /*PRIVATE METHODS END*/

  $.fn.handleThumbs = function (method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.error('����� � ������ ' + method + ' �� ���������� ��� jQuery.smartGrid');
    }
  };

})(jQuery);