(function ($) {
  var methods = {
    init: function (options) {

      var settings = $.extend({
        'maxContainerWidth': '1000',
        'minContainerWidth': '500'
      }, options);

      var $me = $(this);
      /*heightFix*/
      var $canvas = $me.find('canvas');
      resetDimensions($canvas);
      var $toolbar = $('body').find('.toolbar');
      var $canvasCaption = $me.find('.document-viewer__document-caption');
      var $canvasPager = $me.find('.document-viewer__document-pager');
      var heightFix = $toolbar.height() + $canvasCaption.height() + $canvasPager.height();
      /*heightFix*/
      var $canvasContainers = $me.find('.document-viewer__container');
      var initialState = {
        settings: settings,
        fixes: {
          heightFix: heightFix
        }
      };
      services.setState(initialState);
      handleCanvas($canvasContainers, $me);
      $(window).on('resize', function () {
        onWindowResize($canvasContainers, $me);
      });
    }
  };

  var services = {
    getState: function () {
      console.log($(window).data('state'));
      return $(window).data('state');
    },
    setState: function (state) {
      $(window).data('state', state);
      console.log(services.getState());
    }
  };

  /*PRIVATE METHODS*/
  /* Handlers */

  function handleCanvas($canvasContainers, $me) {
    /*heightFix - few widths calculations from initial, will be refactored later, sorry*/

    var state = services.getState();
    var windowSize = getDimensions($(window));
    var $canvas = $canvasContainers.find('canvas');
    var canvasHeight = windowSize.height-state.fixes.heightFix;
    var $toolbar = $('body').find('.toolbar');

    if ($($canvasContainers[1]).is(':visible') === true) {

      $canvas.css({
        display: 'block',
        width: '100%'
      });
      $canvas.css({
        height: getDimensions($canvas).width
      });
    }
    else {
      $canvas.css({display: 'block'});
      $toolbar.css({'justify-content': 'flex-start'});
      handleWidth($canvas, canvasHeight);
      handleHeight($canvas, canvasHeight);
    }
  }
  function handleWidth($el, width) {
    return $el.css({width: width})
  }
  function handleHeight($el, height) {
    return $el.css({height: height})
  }
  /* Getters / Setters*/
  function getDimensions(el) {
    return {
      width: $(el).innerWidth(),
      height: $(el).innerHeight()
    }
  }
  function resetDimensions(el) {
    return $(el).css({
      width: '0',
      height: '0',
      display: 'none'
    });
  }

  /* Events */
  function onWindowResize($canvasContainers, $me) {
    handleCanvas($canvasContainers, $me);
  }

  /*PRIVATE METHODS END*/

  $.fn.slider = function (method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.error('����� � ������ ' + method + ' �� ���������� ��� jQuery.slider');
    }
  };

})(jQuery);