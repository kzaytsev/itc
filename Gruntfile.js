module.exports = function (grunt) {

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.initConfig({

    less: {
      build: {
        files: {
          'output/css/app.css': 'source/less/app.less'
        }
      }
    },

    copy: {
      main: {
        files: [
          {expand: true, cwd: 'source/js/', src: ['**/*'], dest: 'output/js/'},
          {expand: true, cwd: 'source/fonts/', src: ['**/*'], dest: 'output/fonts/'},
          {expand: true, cwd: 'source/img/', src: ['**/*'], dest: 'output/img/'}

        ]
      },
      getBootstrap: {
        files: [
          {expand: true, cwd: 'source/css/', src: ['bootstrap.min.css'], dest: 'output/css/'}

        ]
      },
      postResources: {
        files: [
          {expand: true, cwd: 'output/css', src: ['app.css'], dest: 'd:/Projects/MAERZ.DocumentViewer/MAERZ.DocumentViewer.Web/Content/css/'},
          {expand: true, cwd: 'output/fonts/itc', src: ['*'], dest: 'd:/Projects/MAERZ.DocumentViewer/MAERZ.DocumentViewer.Web/Content/fonts/itc/'},
          {expand: true, cwd: 'output/fonts/roboto', src: ['*'], dest: 'd:/Projects/MAERZ.DocumentViewer/MAERZ.DocumentViewer.Web/Content/fonts/roboto/'}
        ]

      }
    },
    autoprefixer: {
      options: {
        browsers: ['> 1%', 'last 2 versions', 'Safari >= 5']
      },
      your_target: {
        src: 'output/css/app.css'
      }
    },

    includes: {
      build: {
        files: [
          {
          cwd: 'source/blocks/',
          src: 'login.html',
          dest: 'output/'
        },
          {
            cwd: 'source/blocks/',
            src: 'search.html',
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'pdf-view.html',
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'pdf-view_multiple.html', //Main app structure
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'pdf-view_multiple_panel-hidden.html', //Main app structure
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'index.html', //Main app structure
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'mpi_*.html', //MPI
            dest: 'output/'
          },
          {
            cwd: 'source/blocks/',
            src: 'dashboard.html', // App dashboard
            dest: 'output/'
          }
        ]
      }
    },

/*    copy: {
      dev: {
          files: [
            {expand: true, src: ['path/!*'], dest: 'dest/', filter: 'isFile'},
          ]
      }
    },*/
    //---CONNECT---
    connect: {
      server: {
        options: {
          port: 888,
          base: 'output',
          keepalive: false
        }
      }
    },
    watch: {
      files: ['source/**/*'],
      tasks: ['less', 'autoprefixer', 'includes', 'copy']
    }
  });


  grunt.registerTask('build', [
    'less',
    'autoprefixer',
    'includes',
    'copy',
    'connect',
    'watch'
  ]);

  grunt.registerTask('default', ['build']);
};
